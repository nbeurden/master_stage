import torch
import numpy as np
import h5py

def convert_data(data):
    MeV_to_GeV = 1e-3

    n_data = data.shape[0]

    data_ids = torch.nn.functional.one_hot(data[:,:18].long())

    data_aux = data[:,18:20]

    data_e = data[:,20:21]

    data_pt = data[:,21:22]

    data_momenta = data[:, 20:].reshape(data.shape[0], 18, 4)

    data_four_vec = torch.zeros_like(data_momenta)
    data_four_vec[:,:,0] = MeV_to_GeV*torch.exp(data_momenta[:,:,1])*torch.cos( data_momenta[:,:,3])
    data_four_vec[:,:,1] = MeV_to_GeV*torch.exp(data_momenta[:,:,1])*torch.sin( data_momenta[:,:,3])
    data_four_vec[:,:,2] = MeV_to_GeV*torch.exp(data_momenta[:,:,1])*torch.sinh(data_momenta[:,:,2])
    data_four_vec[:,:,3] = MeV_to_GeV*torch.exp(data_momenta[:,:,0])

    data_id_int = data[:,:18].long()

    data_four_vec[data_momenta == 0.] = 0.

    data_ids = torch.transpose(data_ids, 1, 2)
    data_momenta = torch.transpose(data_momenta, 1, 2)
    data_four_vec = torch.transpose(data_four_vec, 1, 2)

    data_tokens = torch.cat((data_momenta, data_ids), dim=1)

    data_mask = (data_momenta[:,0,:] != 0.).unsqueeze(1)

    return data_aux, data_tokens, data_four_vec, data_id_int, data_mask

   
def get_training_dataloaders(data_file, batch_size, n_train_data=-1, dropout=None):
    # Load all the data
    hf = h5py.File(data_file, 'r')
    # print("hf", hf)
    # print("keys", hf.keys())

    data_train   = torch.tensor(np.array(hf.get('X_train')), dtype=torch.float32)
    data_val     = torch.tensor(np.array(hf.get('X_val')), dtype=torch.float32)
    data_test    = torch.tensor(np.array(hf.get('X_test')), dtype=torch.float32)
    labels_train = torch.tensor(hf.get('Y_train'), dtype=torch.long)
    labels_val   = torch.tensor(hf.get('y_val'), dtype=torch.long)
    labels_test  = torch.tensor(hf.get('y_test'), dtype=torch.long)

    # Drop data
    if dropout is not None:
        data_train = data_train[dropout]
        labels_train = labels_train[dropout]

    # Limit amount of training data
    if n_train_data > 0:
        data_train = data_train[:n_train_data]
        labels_train = labels_train[:n_train_data]

    # Normalize
    data_aux_train, data_tokens_train, data_momenta_train, data_id_int_train, data_mask_train = convert_data(data_train)
    data_aux_val,   data_tokens_val,   data_momenta_val,   data_id_int_val,   data_mask_val,  = convert_data(data_val)
    data_aux_test,  data_tokens_test,  data_momenta_test,  data_id_int_test,  data_mask_test  = convert_data(data_test)

    # Combine
    data_train_combined = torch.utils.data.TensorDataset(data_aux_train, data_tokens_train, data_momenta_train, data_id_int_train, data_mask_train, labels_train)
    data_val_combined   = torch.utils.data.TensorDataset(data_aux_val,   data_tokens_val,   data_momenta_val,   data_id_int_val,   data_mask_val,   labels_val)
    data_test_combined  = torch.utils.data.TensorDataset(data_aux_test,  data_tokens_test,  data_momenta_test,  data_id_int_test,  data_mask_test,  labels_test)
    
    # Make loader
    train_loader = torch.utils.data.DataLoader(dataset=data_train_combined, batch_size=batch_size, shuffle=True)
    val_loader   = torch.utils.data.DataLoader(dataset=data_val_combined,   batch_size=batch_size)
    test_loader  = torch.utils.data.DataLoader(dataset=data_test_combined,  batch_size=batch_size)


    return train_loader, val_loader, test_loader

