from keras import layers
import tensorflow as tf
import numpy as np
#from WordEmbeddingLayer import WordEmbeddingLayer
#from PositionalEmbeddingLayer import PositionalEmbeddingLayer

class PositionEmbeddingFixedWeights(layers.Layer):
    """Generates a Positional Embedding layer with fixed weights which enherits the keras layers methods.
    The Positional embedding layer """
    def __init__(self, sequence_length, vocab_size, output_dim, **kwargs):
        super(PositionEmbeddingFixedWeights, self).__init__(**kwargs)
        word_embedding_matrix = self.get_position_encoding(vocab_size, output_dim)   
        position_embedding_matrix = self.get_position_encoding(sequence_length, output_dim)          
        #self.vectorization_layer = layers.TextVectorization(output_sequence_length = sequence_length, max_tokens=vocab_size)                             
        self.word_embedding_layer = layers.Embedding(input_dim=vocab_size, output_dim=output_dim, weights=[word_embedding_matrix], trainable=False)
        self.position_embedding_layer = layers.Embedding(input_dim=sequence_length, output_dim=output_dim, weights=[position_embedding_matrix], trainable=False)

             
    def get_position_encoding(self, seq_len, d, n=10000):
        P = np.zeros((seq_len, d))
        for k in range(seq_len):
            for i in np.arange(int(d/2)):
                denominator = np.power(n, 2*i/d)
                P[k, 2*i] = np.sin(k/denominator)
                P[k, 2*i+1] = np.cos(k/denominator)
        return P
 
 
    def call(self, inputs): 
        #vectorized_phrases = self.vectorization_layer(inputs)       
        position_indices = tf.range(tf.shape(inputs)[-1])
        embedded_words = self.word_embedding_layer(inputs)
        embedded_indices = self.position_embedding_layer(position_indices)
        return embedded_words + embedded_indices