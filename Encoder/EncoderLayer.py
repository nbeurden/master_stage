from keras import layers
from MultiHeadAttentionLayer import MultiHeadAttentionLayer
from FeedForwardLayer import FeedForwardLayer
from AddNormLayer import AddNormLayer

class EncoderLayer(layers.Layer):
    """Generates an Encoder layer which enherits from keras layers.
    It takes a matrix x, padding mask and training and returns the 
    output of the encoder layer."""
    def __init__(self, h, d_k, d_v, d_ff, d_model, rate,**kwargs):
        # Initialize
        super(EncoderLayer, self).__init__(**kwargs)   
        # Generate layers needed
        self.multihead_attention = MultiHeadAttentionLayer(h, d_k, d_v, d_model)
        self.dropout1 = layers.Dropout(rate)
        self.addnorm1 = AddNormLayer()
        self.feedforward = FeedForwardLayer(d_ff, d_model)
        self.dropout2 = layers.Dropout(rate)
        self.addnorm2 = AddNormLayer()

    def __call__(self, x, padding_mask, training):
        # x = inputs of 'Encoder layer' = positionalembedingfixedweights output
        # Each layers output is the input of the next layer
        multihead_output = self.multihead_attention(x, x, x, padding_mask) 
        dropout1_output = self.dropout1(multihead_output, training=training) 
        addnorm1_output = self.addnorm1(dropout1_output, x)

        feedforward_output = self.feedforward(addnorm1_output)
        dropout2_output = self.dropout2(feedforward_output, training=training) 
        addnorm2_output = self.addnorm2(dropout2_output, addnorm1_output)

        return addnorm2_output