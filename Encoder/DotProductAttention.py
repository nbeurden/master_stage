import tensorflow as tf
from keras import layers
from keras import backend

# Implementing the Scaled-Dot Product Attention - subclass of 'Layer'
class DotProductAttention(layers.Layer): 
    """Generate a Dot-Product Attention layer which enherits the keras layers methods.
    The Dot-Product Attention layer takes Queries, Keys and Values matrices, the dimension
    of the Queries matrix and a mask as input variables and returns the attention."""
    def __init__(self, **kwargs): 
        #Initialize
        super(DotProductAttention, self).__init__(**kwargs)
    
    def call(self, queries, keys, values, d_k, mask=None): 
        # if (Q,K,V) = (X,X,X) we are dealing with selfattention
        # Compute scores based on  'Attention is all you need'
        scores = tf.matmul(queries, keys, transpose_b=True) / tf.math.sqrt(tf.cast(d_k, tf.float32))

        # Apply mask
        if mask is not None: 
            scores += -1e9 * mask 
        
        # Computing the weights by a softmax operation
        weights = backend.softmax(scores)
        
        # Computing the attention
        attention = tf.matmul(weights, values)
        
        return attention