from keras import layers

class FeedForwardLayer(layers.Layer): 
    """Generates a FeedForward layer which enherits the keras layers methods.
    A fully connected feed-forward network consists of two linear transformations 
    with a relu activation in between. The first linear transformation produces an 
    output dimensionality d_ff=2048 while the second linear transformation 
    produces an output of d_model = 512 (Attention is all you need)"""

    def __init__(self, d_ff, d_model, **kwargs): 
        # Initialize
        super(FeedForwardLayer, self).__init__(**kwargs)
        # Create needed layers
        self.fully_connected1 = layers.Dense(d_ff)
        self.fully_connected2 = layers.Dense(d_model)
        self.activation = layers.ReLU()
    
    def __call__(self, input): 
        # Input is passed into the 2 fully-connected layers with activation between.
        after_first_layer = self.fully_connected1(input)
        after_activation = self.activation(after_first_layer)
        after_second_layer = self.fully_connected2(after_activation)

        return after_second_layer