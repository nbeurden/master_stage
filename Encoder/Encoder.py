from keras import layers
from keras import Model
import tensorflow as tf
import numpy as np 
from EncoderLayer import EncoderLayer
from PositionalEmbeddingFixedWeights import PositionEmbeddingFixedWeights

class Encoder(Model): 
    """Generates an Encoder model which enherits from keras Model.
    It takes inputs and generates the encoder output."""
    def __init__(self, batch_size, seq_len, num_heads, num_layers, d_model, d_ff, d_k, d_v, rate, vocab_size, output_length, padding_mask, training, **kwargs): 
        # Initialize
        super(Encoder, self).__init__(self, **kwargs)
        self.batch_size = batch_size
        self.seq_len = seq_len
        self.num_heads = num_heads
        self.num_layers = num_layers
        self.d_model = d_model
        self.d_ff = d_ff
        self.d_k = d_k
        self.d_v = d_v
        self.rate = rate 
        self.vocab_size = vocab_size
        self.output_length = output_length
        self.padding_mask = padding_mask
        self.training = training
    
        # Generate Needed Layers 
        self.pos_encoding = PositionEmbeddingFixedWeights(self.output_length, self.vocab_size, self.output_length)
        self.encoder_layers = [EncoderLayer(num_heads, d_k, d_v, d_ff, d_model, rate) for _ in range (num_layers)]
        self.output_layer = layers.Dense(seq_len)
        self.queries_matrix = np.random.random((batch_size, seq_len, d_k))

    def call(self, inputs):

        pos_encoding_output = self.pos_encoding(inputs)
        x = pos_encoding_output

        for encoder_layer in self.encoder_layers:
            x = encoder_layer(x, self.padding_mask, self.training)

        output = self.output_layer(x)
        return output

        


