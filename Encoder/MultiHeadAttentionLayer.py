import DotProductAttention
import tensorflow as tf 
from keras import layers

class MultiHeadAttentionLayer(layers.Layer): 
    """Generates a Multi-Head Attention layer which enherits the keras layers methods.
    Projection matrix of Queries, Keys and Values vectors are reshaped such that 
    Dot-Porduct Attention layes can be run in parallel."""
    def __init__(self, h, d_k, d_v, d_model, **kwargs):
        # Initialize
        super(MultiHeadAttentionLayer, self).__init__(**kwargs)
        
        # Create needed layers
        self.attention = DotProductAttention.DotProductAttention() # Scaled dot product attention

        # Define layer parameters
        self.heads = h # Number of attention heads to use
        self.d_k = d_k # Dim of the linearly projected quaries and keys
        self.d_v = d_v # Dim of the linearly projected values 
        self.d_model = d_model # Dim of the model

        self.W_q = layers.Dense(d_k) # Learned projection matrix for the queries 
        self.W_k = layers.Dense(d_k) # Learned projection matrix for the keys 
        self.W_v = layers.Dense(d_v) # Learned projection matrix for the values
        self.W_o = layers.Dense(d_model) # Learned projection matrix for multi-head output

    def reshape_tensor(self, x, heads, flag):
        """Reshape linearly projected queries, keys and values in such a manner 
        as to allow the attention heads to be computed in parallel.
        x: Q, K, V Tensor of shape (batch_size, seq_length, d_k/v) to reshape
        flag: Rearranging direction, True: False: """ 
        
        if flag: 
            # Tensor shape after reshaping and transposing: (batch_size, heads, seq_length, -1)
            # From Q, K, V to reshaped projection matrix
            x = tf.reshape(x, shape=(tf.shape(x)[0], tf.shape(x)[1], heads, -1))
            x = tf.transpose(x, perm=(0,2,1,3))
        else: 
            # Reverting the reshaping and transposing operations:  (batch_size, seq_length, d_k) 
            # From reshaped projection matrix to original Q, K, V
            x = tf.transpose(x, perm = (0,2,1,3))
            x = tf.reshape(x, shape=(tf.shape(x)[0], tf.shape(x)[1], self.d_k))
        return x

    def __call__(self, queries, keys, values, mask=None): 
        print(queries)
        # Rearrange the queries to be able to compute all heads in parallel
        q_reshaped = self.reshape_tensor(self.W_q(queries), self.heads, True)
        k_reshaped = self.reshape_tensor(self.W_k(keys), self.heads, True)
        v_reshaped = self.reshape_tensor(self.W_v(values), self.heads, True)

        # Compute the multi-head output.
        o_reshaped = self.attention(q_reshaped, k_reshaped, v_reshaped, self.d_k, mask)

        # Rearrange back the output into concatenated form
        output = self.reshape_tensor(o_reshaped, self.heads, False)

        return self.W_o(output)
