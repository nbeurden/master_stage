from keras import layers

class AddNormLayer(layers.Layer):
    """Generate a AddNorm layer which enherits the keras layers methods.
    The AddNormlayer two matrices as input, adds them and then applies layer normalization to this sum.
    Two input matrices should be of the same shape, output is of the same shape as input matrices."""
    def __init__(self, **kwargs):
        # Initialize
        super(AddNormLayer, self).__init__(**kwargs)
        # keras layer normalization layer 
        self.layer_norm = layers.LayerNormalization() #Layer normalization layer
    
    def __call__(self, x, sublayer_x): 
        # The layer and sublayer input should be of the same size to be summed.
        add = x + sublayer_x
        # Apply layer normalization to the sum
        return self.layer_norm(add)   