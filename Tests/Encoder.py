from keras import layers
from keras import Model
import tensorflow as tf
import numpy as np 
from EncoderLayer import EncoderLayer
from PositionalEmbeddingFixedWeights import PositionalEmbeddingFixedWeights

class Encoder(Model): 
    def __init__(self, batch_size, seq_len, num_heads, num_layers, d_model, d_ff, d_k, d_v, rate, **kwargs): 
        super().__init__(**kwargs)
        self.batch_size = batch_size
        self.seq_len = seq_len
        self.num_heads = num_heads
        self.num_layers = num_layers
        self.d_model = d_model
        self.d_ff = d_ff
        self.d_k = d_k
        self.d_v = d_v
        self.rate = rate 

        self.pos_encoding = PositionalEmbeddingFixedWeights()
        self.encoder_layers = [EncoderLayer(num_heads, d_k, d_v, d_ff, d_model, rate) for _ in range (num_layers)]
        self.output_layer = layers.Dense(seq_len)
        self.queries_matrix = np.random.random((batch_size, seq_len, d_k))

    def __call__(self, vocab_size, output_dim, inputs, padding_mask, training=True):
        pos_encoding_output = self.pos_encoding(self.seq_len, vocab_size, output_dim, inputs)
        


        #print('type pos_enc = ', type(pos_encoding_output), tf.shape(pos_encoding_output), pos_encoding_output[0])
        #print('type inputs = ', type(inputs), tf.shape(inputs), inputs[0])
        #print(self.encoder_layers)
        #print('Pos_encoding_shape = ', pos_encoding_output.shape)
    
        x = pos_encoding_output

        for encoder_layer in self.encoder_layers:
             
            x = encoder_layer(x, padding_mask, training)
            #layer_inputs = self.queries_matrix
            #print('layer_inputs = ', layer_inputs.shape)
            
            #print('In loop')
            #layer_inputs = encoder_layer(layer_inputs, pos_encoding_output)
            #print('input done')

        #output = self.output_layer(inputs)
        output = self.output_layer(x)

        return output
"""
class Encoder(Model): 
    def __init__(self, seq_len, vocab_size, d_model, h, d_k, d_v, d_ff, rate, n, inputs, **kwargs): 
        super().__init__(**kwargs) 
        self.pos_encoding = PositionalEmbeddingFixedWeights()
        self.pos_encoding = self.pos_encoding(seq_len, vocab_size, d_model, inputs)
        self.dropout = layers.Dropout(rate)
        self.encoder_layer = [EncoderLayer(h, d_k, d_v, d_model, d_ff, rate) for _ in range(n)]
        self.output_layer = layers.Dense(seq_len)

    def __call__(self, input_sentence, training): 
        pos_encoding_output = self.pos_encoding(input_sentence)
        x = self.dropout(pos_encoding_output, training=training)
        
        for encoder_layer in self.encoder_layer:
            x = EncoderLayer(x, training)

        output = self.output_layer(x)
        return output
"""