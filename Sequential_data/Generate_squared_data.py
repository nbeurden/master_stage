import numpy as np
import pandas as pd
import tensorflow as tf
import argparse
import time


rng = np.random.default_rng()

def square(n): 
    return int(n**2)


# use the argparse package to parse command line arguments
parser = argparse.ArgumentParser(description='Returns series of generated squared values')
parser.add_argument('-N', type=int, default=10000, help='Number of events')
parser.add_argument('-n', type=int, default=7, help='Number of generated values per event')
parser.add_argument('-s1', type=int, default=0, help='Start squaring at random integer between s1 and s2')
parser.add_argument('-s2', type=int, default=6, help='Start squaring at radom integer between s1 and s2')
parser.add_argument('-f', help='Output filename')
args = parser.parse_args()

# fix parameters
N_events = args.N
n_values = args.n


if args.f is None:
    # construct a filename from the parameters plus a timestamp (to avoid overwriting)
    output_filename = "Squared_data_{}_events_{}_values_at_{}.csv".format(N_events, n_values, time.strftime("%Y%m%d%H%M%S"))
else: 
    output_filename = args.f


square_data = [] 
for event in range (0,N_events): 
    sub_values = []
    
    start = rng.integers(args.s1,args.s2)
    end = start+n_values
    
    for i in range(start,end):
        sub_values.append(square(i))

    square_data.append(sub_values)

df_generated_data = pd.DataFrame(square_data)
df_generated_data.to_csv("C:\\Users\\nvanb\\Documents\Master\\1_Masterstage\\master_stage\\Sequential_data\\{}".format(output_filename), index=False)