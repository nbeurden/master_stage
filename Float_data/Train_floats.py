import sys 
from pathlib import Path
import tensorflow as tf
import pandas as pd
import numpy as np 

module_path = str(Path.cwd().parents[0]/'Encoder')
if module_path not in sys.path:
    sys.path.append(module_path)

from Encoder import Encoder

file_path = "/home/nbeurden/Master Stage/Float_data/Float_data_10000_events_25_values_at_20240318190554.csv"

def train_val_test_split(df):
    # np.split() to split the dataset
    # train_znorm.sample(frac=1, random_state=42) to shuffle the dataset random
    # [int(.6*len(train_znorm)),int(.8*len(train_znorm))] first split after 60% of the data. second split after 80%
    # 60% train set - 20% validation set - 20% test set
    df_train, df_validate, df_test = np.split(df.sample(frac=1, random_state=42), [int(.6*len(df)),int(.8*len(df))])
    return df_train, df_validate, df_test

def define_targets_and_features(df): 
    """Function defines the features and targets of a dataframe,
    The targets are in the last column of the dataframe; the features are in the other columns"""
    
    features, targets = df.keys()[0:-1], df.keys()[1:]
    return features, targets

df_data = pd.DataFrame(pd.read_csv(file_path))
unique_values = np.unique(df_data)
df_integers = df_data

eps = 0.01
counter = 1 
for value in unique_values: 
    for column in df_data.columns:
        df_integers[column] = np.where((value-eps <= df_data[column]) & (df_data[column] <= value+eps), counter, df_data[column])
    counter += 1 

df_integers = df_integers.astype(int)


# Make train, validation and test split
df_train, df_validate, df_test = train_val_test_split(df_integers)

# Store test to .csv file
df_test.to_csv("/home/nbeurden/Master Stage/Float_data/fixed_length_testdata_2.csv")


# Define targets and features
features, targets = define_targets_and_features(df_integers)
train_features = tf.constant(df_integers[features])
train_targets = tf.constant(df_integers[targets])
validate_features = tf.constant(df_integers[features])
validate_targets = tf.constant(df_integers[targets])

d_model = 512
d_ff = 2048
batch_size = 64
num_heads = 8 
num_layers = 6
d_k = int(d_model/num_heads)
d_v = int(d_model/num_heads)
rate = 0.1

n_events = df_integers.shape[0]
vocab_size = int(np.max(np.max(df_data), axis=0))+1 # add one because we include 0
seq_len = len(df_integers.columns)+vocab_size
output_length = 512
print(vocab_size)

padding_mask = None

# Define the loss function and optimizer
loss_function = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
#loss_function = tf.keras.losses.CategoricalCrossentropy()
optimizer = tf.keras.optimizers.Adam()

encoder_model = Encoder(batch_size, seq_len, num_heads, num_layers, d_model, d_ff, d_k, d_v, rate, vocab_size, output_length, padding_mask, training=True)
encoder_model.build(input_shape=(n_events, vocab_size))
encoder_model.summary()

num_epochs = 5
encoder_model.compile(optimizer, loss=loss_function, metrics='accuracy')
history = encoder_model.fit(train_features, train_targets, epochs=num_epochs, validation_data=(validate_features, validate_targets))

encoder_model.save_weights("/home/nbeurden/Master Stage/Float_data/model_fixedlen.keras")



