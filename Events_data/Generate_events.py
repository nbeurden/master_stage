import numpy as np
import pandas as pd
import tensorflow as tf
import argparse
import time

rng = np.random.default_rng()

def coordinate(t):
    x = 10*t
    y = 0.0125*t
    z = t
    return x, y, z


# use the argparse package to parse command line arguments
parser = argparse.ArgumentParser(description='Returns series of generated squared values')
parser.add_argument('-N', type=int, default=10000, help='Number of events')
parser.add_argument('-t', type=int, default=10, help='Number of generated values per event')
parser.add_argument('-s1', type=int, default=0, help='Start sine at random integer between s1 and s2')
parser.add_argument('-s2', type=int, default=4, help='Start sine at radom integer between s1 and s2')
parser.add_argument('-f', help='Output filename')
args = parser.parse_args()

# fix parameters
N_events = args.N
n_values = args.t


if args.f is None:
    # construct a filename from the parameters plus a timestamp (to avoid overwriting)
    output_filename = "Events_data_{}_events_{}_values.csv".format(N_events, n_values)
else: 
    output_filename = args.f


coordinate_data = [] 
for event in range (0,N_events): 
    sub_values = []
    
    start = rng.integers(args.s1,args.s2)
    end = start+n_values
    
    for dim in range (0,3):
        for i in range (start, end):
            coord = coordinate(i)
            sub_values.append(coord[dim])

    coordinate_data.append(sub_values)

df_generated_data = pd.DataFrame(coordinate_data)
df_generated_data.to_csv("C:\\Users\\nvanb\\Documents\Master\\1_Masterstage\\master_stage\\Events_data\\{}".format(output_filename), index=False)
